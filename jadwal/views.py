from django.shortcuts import render
from django.views.generic import CreateView
from django.http import HttpResponse, HttpResponseRedirect
from .models import JadwalPribadi
from .forms import Message_Form
from django.shortcuts import redirect

response = {'author' : 'Jose Devian'}
# Create your views here.
def schedule(request):
    return render(request, 'schedule.html', {'form': form})

def post_jadwal(request):
    form = Message_Form(request.POST or None)
    if(request.method == "POST"):
        response["nama"] = request.POST["nama"]
        response["hari"] = request.POST["hari"]
        response["tangga"] = request.POST["tanggal"]
        response["jam"] = request.POST["jam"]
        response["tempat"] = request.POST["tempat"]
        response["kategori"] = request.POST["kategori"]

        jadwal_pribadi = JadwalPribadi(nama = response["nama"], hari = response["hari"], tanggal = response["tanggal"], jam = response["jam"], tempat = response["tempat"], kategori = response["kategori"])
        jadwal_pribadi.save()
        jadwal = JadwalPribadi.objects.all()
        response['jadwal'] = jadwal
        return render(request, "schedule.html", response)

    else:
        return render(request, "schedule.html", response)

def jadwal_response(request):
    response["jadwal"] = JadwalPribadi.objects.all().values()
    return render(request,"schedule.html",response)

def jadwal_submission(request):
    html = "schedule_form.html"
    response["message_form"] = Message_Form
    return render(request, html, response)

def delete_response(request):
    jadwal  = JadwalPribadi.objects.all().delete()
    response["jadwal"] = jadwal
    return render(request,"schedule.html", response)
