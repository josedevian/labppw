from django import forms
from jadwal.models import JadwalPribadi

class Message_Form(forms.Form):
    hari = forms.CharField(label = "Day")
    tanggal = forms.DateField(label = "Date:", widget = forms.DateInput(attrs={'type' : 'date'}))
    jam = forms.TimeField(label = "Time:", widget = forms.TimeInput(attrs={'type' : 'time'}))
    nama_kegiatan = forms.CharField(label = "Name")
    tempat = forms.CharField(label = "Place:")
    kategori = forms.CharField(label = "Category:")
