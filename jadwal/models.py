from django.db import models
from django import forms

# Create your models here.

class JadwalPribadi(models.Model):
    hari = models.CharField(max_length=30)
    tanggal = models.DateField()
    jam = models.TimeField()
    nama_kegiatan = models.CharField(max_length=50)
    tempat = models.CharField(max_length=50)
    kategori = models.CharField(max_length=50)
