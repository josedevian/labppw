"""LabPPW URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from landingpage.views import home
from contact.views import contact
from portofolio.views import portofolio
from jadwal.views import jadwal_response
from jadwal.views import post_jadwal
from jadwal.views import delete_response
from jadwal.views import jadwal_submission

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home, name="home"),
    path('contact/', contact, name="contact"),
    path('portofolio/', portofolio, name="portofolio"),
    path('jadwal_submission/', jadwal_submission, name = 'jadwal_submission'),
    path('jadwal_response/', jadwal_response, name = 'jadwal_response'),
    path('post_jadwal/', post_jadwal, name = 'post_jadwal'),
    path('delete_response/', delete_response, name = 'delete_response'),
]
